extends Object
class_name Constant

#region METRICS
const PRESSURE_KPA_NAME := "kPa"
const PRESSURE_KPA_VALUE := 1.0
const PRESSURE_ATM_NAME := "atm"
const PRESSURE_ATM_VALUE := 0.00986923

const TEMPERATURE_K_NAME := "K"
const TEMPERATURE_K_VALUE := 1

#endregion

#region THRESHOLDS
const THRESH_PRESSURE_LOW := PRESSURE_KPA_VALUE * 45
const THRESH_TEMPERATURE_LOW := TEMPERATURE_K_VALUE * 283
#endregion
