extends Node
class_name ModulatorComponent
##Modulates the color of a parent based on the value of the referenced property 
##The color remains "stable" as long as it doesn't cross any thresholds.

@export var object_reference: Node
@export var object_property_path: NodePath = ^"text"

@export var color_above_thres: Color = Color.GREEN
@export var color_below_thres: Color = Color.RED
@export var color_stable_thres: Color = Color.WHITE

@export var thres_low: float = 10
@export var thres_high: float = 100
@export var thres_tolerance: float = 1.0

@export var interpolate_color: bool = true

func _enter_tree() -> void:
	if get_parent().has_signal("draw"):
		object_reference.draw.connect(update_color)
	else:
		push_error("Invalid parent")


func validate_values() -> bool:
	if not thres_high > thres_low:
		push_error("High threshold must be ACTUALLY higher.")
		return false
		
	return true
		

func update_color():
	validate_values()
	if get_parent().get("modulate") == null:
		push_error("Invalid parent")
		return
		
	get_parent().modulate = get_appropiate_color()


func get_value() -> float:
	if not object_reference:
		push_error("Object has not been set")
		return 0
		
	var val: String = str(object_reference.get_indexed(object_property_path))
	var value: float = val.to_float()
	return value
	

func get_appropiate_color() -> Color:
	var value: float = get_value()
	
	if value > thres_high:
		return color_above_thres
	elif value < thres_low:
		return color_below_thres 
	else:
		if not interpolate_color:
			return color_stable_thres
			
		var mid_value: float = (thres_low + thres_high) / 2
		var first_color: Color = color_stable_thres
		var second_color: Color
		
		var normalized_val: float
		if value >= mid_value:
			second_color = color_above_thres
			normalized_val = remap(value, mid_value, thres_high, 0, 1)
		elif value < mid_value:
			second_color = color_below_thres
			normalized_val = remap(value, mid_value, thres_low, 0, 1)
			
		var final_val: Color = first_color.lerp(second_color, normalized_val)
		return final_val
		
