extends StaticBody3D
class_name VirtualTouchScreen3D
## A StaticBody3D that creates a small screen that mirrors a Viewport of your choice.
## You can interact with the mirrored viewport by clicking on it.

const DEFAULT_RESOLUTION_PER_UNIT: int = 512

const INVALID_VECTOR2 := Vector2.INF


## The Viewport or SubViewport that will be displayed in this screen.
@export var ref_viewport:Viewport:
	set(val):
		if val is Viewport:
			ref_viewport = val
			#if cursor_layer.get_parent(): 
				#cursor_layer.get_parent().remove_child(cursor_layer)
			#ref_viewport.add_child(cursor_layer)
			#cursor_layer.ref_viewport = ref_viewport
			refresh_material.call_deferred()

## Direct reference to the MeshInstance3D node in this scene that will be used, if none is set, one will be created on _ready().
## This is only for the node, the mesh that it will hold is created on runtime and will replace any already set.
@export var ref_mesh_instance: MeshInstance3D 

## Texture used for the cursor inside the screen. Can be left empty.
@export var cursor_texture:Texture#TEMP

## A reference to the CanvasLayer that parents the cursor, use cursor_layer.cursor to access the Sprite2D of the cursor
var cursor_layer := CursorLayer.new(ref_viewport, cursor_texture)
var sub_screen_mouse_pos:Vector2


func _ready() -> void:
	refresh_material.call_deferred()
	cursor_layer.ref_viewport = ref_viewport
	
	mouse_entered.connect(Callable(cursor_layer,"set").bind("showCursor",true))
	mouse_exited.connect(Callable(cursor_layer,"set").bind("showCursor",false))
	
func _input_event(camera: Camera3D, event: InputEvent, eventPos: Vector3, normal: Vector3, shape_idx: int) -> void:
	var local_mouse_pos: Vector3 = to_local(eventPos)
	var mouse_vec2_pos: Vector2 = Vector2(local_mouse_pos.x, local_mouse_pos.y) 
	var mesh_size: Vector2 = get_mesh_size()/2
	var mouse_uv_pos: Vector2 = Vector2(mouse_vec2_pos.x + mesh_size.x, abs(mouse_vec2_pos.y - mesh_size.y)) / 2 #floats from 0 to 1. Percentage of position on the screen
	sub_screen_mouse_pos = mouse_uv_pos * ref_viewport.get_visible_rect().size
	if event is InputEventMouse:
		event.position = sub_screen_mouse_pos
	ref_viewport.push_input(event)
	
	
	
func _process(delta: float) -> void:
	cursor_layer.cursor.position = sub_screen_mouse_pos


## Refreshes the ViewportTexture used by the material. Called automatically when setting a new ref_viewport. 
func refresh_material():
	if ref_viewport==null: 
		push_error("Cannot refresh material, there is no ref_viewport set.")
		return
		
	var _texture: ViewportTexture = ref_viewport.get_texture() 
		
	var new_mat := StandardMaterial3D.new()
	new_mat.albedo_texture = _texture
	ref_mesh_instance.set_surface_override_material(0, new_mat)
		
	print_debug(str(self.get_name()) + "\n" 
	+ str(ref_viewport) + "\n" 
	+ str(ref_mesh_instance.mesh.material) + "\n" 
	+ str(ref_viewport.world_2d))
	
	
func adjust_viewport_resolution(ratio: float):
	var mesh_size: Vector2 = get_mesh_size()
	ref_viewport.size = (Vector2.ONE * DEFAULT_RESOLUTION_PER_UNIT * mesh_size) * ratio
	
		
func get_mesh_size() -> Vector2:
	if not ref_mesh_instance.mesh is PlaneMesh:
		push_error("Needs a PlaneMesh")
		return INVALID_VECTOR2

	return ref_mesh_instance.mesh.size

class CursorLayer extends CanvasLayer:
	var cursor:=Sprite2D.new()
	var ref_viewport:Viewport
	
	var showCursor:bool = true:
		set(val):
			showCursor = val
			cursor.visible = showCursor
			
	func _init(_ref_viewport:Viewport, _cursor_texture:Texture):
		cursor.texture = _cursor_texture
		cursor.centered = false
		ref_viewport = _ref_viewport
		layer += 1
	
	func _ready() -> void:
		
		add_child(cursor)
		
#	func reposition_mouse_from_event(camera:Camera3D,event:InputEvent, eventPos:Vector3, shapeID:int):
#		if event is InputEventMouseMotion:
#			cursor.position 
	
#	func _process(delta: float) -> void:
#		if ref_viewport:
#			cursor.position = ref_viewport.get_mouse_position() / Vector2(mainref_viewport.size)
#			pass
