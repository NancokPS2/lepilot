extends Camera3D

const DEFAULT_ROTATION_LIMIT := Vector3(PI*0.5, PI*0.8, 0)

@export var pivot_node: Node3D

@export var rotation_speed: float = 0.01

@export var rotation_limits: Vector3 = DEFAULT_ROTATION_LIMIT

func _ready() -> void:
	if not pivot_node:
		pivot_node = self


func _input(event: InputEvent) -> void:
	if not Input.is_action_pressed("right_click"):
		return
		
	if event is InputEventMouseMotion:
		var horizontal: float = -event.relative.x
		var vertical: float = -event.relative.y
		pivot_node.rotation += Vector3(vertical, horizontal, 0) * rotation_speed
		pivot_node.rotation = pivot_node.rotation.clamp(-rotation_limits, rotation_limits)
		
	

