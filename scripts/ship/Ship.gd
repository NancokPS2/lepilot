extends Node3D
class_name Ship

enum ControlInputs {
	TOGGLE_WARNING_LIGHTS,
	OPEN_EXTERNAL_DOORS,
}
enum Sections {
	CABIN,
	CARGO,
	ENGINES,
	LIFE_SUPPORT,
	WEAPONS,
}

enum PartStatus {
	ACTIVE,
	UNPOWERED,
	BROKEN,
	MISSING,
}

enum Warnings {
	OVERHEATING,
	UNDERHEATING,
	OVERPRESSURE,
	UNDERPRESSURE,
}

enum Stats {
	HULL,
	POWER,
	HEAT,
	CARGO,
	PRESSURE,
	HEAT_TARGET, ##To WHERE temperature is moving towards
	POWER_USAGE, ##Power missing
}

signal warning_activated(warning: Warnings)
signal warning_deactivated(warning: Warnings)
signal stats_changed()
signal stats_base_changed()
signal part_status_changed()

@export var stats: ShipStats = ShipStats.new()

@export var part_warning_lights: Array[ShipWarningLight]
@export var part_doors: Array[ShipDoor]
@export var part_misc: Array[ShipPart]
@export var control_panels: Array[ShipControlPanel3D]

var stats_current: Dictionary

var memory: Dictionary
var dict_part_status: Dictionary
var dict_warnings: Dictionary

#region SETUP
func _ready() -> void:
	update_composing_nodes()
	stat_reload()


func _process(delta: float) -> void:
	tick(delta)


func update_composing_nodes():
	for node: Node in get_children():
		
		if node is ShipControlPanel3D and not node in control_panels:
			control_panels.append(node)
			node.control_activated.connect(on_control_activated)
			
		elif node is ShipWarningLight and not node in part_warning_lights:
			part_warning_lights.append(node)
#endregion

###LIFETIME###
func tick(delta: float):
	tick_set_stats(delta)
	
	##Power evaluation
	#Turn off random part if not enough power
	if stat_get(Stats.POWER_USAGE) > stat_get(Stats.POWER):
		var random_part: ShipPart = part_get_all().pick_random()
		part_set_status(random_part, PartStatus.UNPOWERED, true)
	
	##Heat normalization
	var curr_heat: float = stat_get(Stats.HEAT)
	var targ_heat: float = stat_get(Stats.HEAT_TARGET)
	stat_change(Stats.HEAT, move_toward(curr_heat, targ_heat, delta*0.1))
	
	##Heat effects
	if stat_get(Stats.HEAT) > stats.heat_max:
		stat_change(Stats.HULL, -1*delta)
	
	##External door processing
	for door: ShipDoor in part_doors:
		if door.external:
			if (
				part_get_status(door, PartStatus.BROKEN) or 
				part_get_status(door, PartStatus.ACTIVE) or 
				part_get_status(door, PartStatus.MISSING)
			):
				stat_change(Stats.PRESSURE, -1*delta)
				stat_change(Stats.HEAT, -1.5*delta)

func tick_set_stats(delta: float):
	##HEAT
	stat_set(Stats.HEAT_TARGET, 50)
	
	##POWER USE
	var power_cons: float
	for part: ShipPart in part_get_all():
		power_cons += part_get_power_consumption(part)
	stat_set(Stats.POWER_USAGE, power_cons)

#region STATS
func stat_reload():
	stat_set(Stats.HULL, stats.hull_max)
	stat_set(Stats.POWER, stats.power_max)
	stat_set(Stats.HEAT, stats.heat_max)
	stat_set(Stats.CARGO, stats.cargo_max)
	stat_set(Stats.PRESSURE, stats.pressure_max)

func stat_set(stat: Stats, value: float):
	stats_current[stat] = value
	
func stat_change(stat: Stats, value:float):
	stats_current[stat] = stat_get(stat) + value
	
func stat_get(stat: Stats) -> float:
	return stats_current.get(stat, 0)
#endregion
	
#region PART SYSTEM
func part_set_status(part: ShipPart, status: PartStatus, add: bool):
	var bit: int = 1<<status
	if add:
		dict_part_status[part] = dict_part_status.get(part, 0) | bit
	else:
		dict_part_status[part] = dict_part_status.get(part, 0) & ~(bit)


func part_get_status(part: ShipPart, status:PartStatus) -> bool:
	var bit: int = 1<<status
	return dict_part_status.get(part, 0) & bit
	

func part_toggle(part: ShipPart, activate: bool):
	if not part.has_method(&"toggle"):
		push_error(str(part) + " does not have a toggle method.")
		return
		
	part.toggle(activate)
	part_set_status(part, PartStatus.ACTIVE, activate)

	
func part_get_power_consumption(part: ShipPart) -> float:
	return part.power_consumption if not part_get_status(part, PartStatus.UNPOWERED) else 0
	
	
func part_get_all() -> Array[ShipPart]:
	var output: Array[ShipPart] = [] 
	for part: ShipPart in part_misc + part_doors + part_warning_lights:
		output.append(part)
	return output
	
#endregion

#region WARNING SYSTEM
func warning_evaluate_all():
	var warnings_to_activate: Array[Warnings] = []
	
	##Heat
	if stat_get(Stats.HEAT) > stats.heat:
		warnings_to_activate.append(Warnings.OVERHEATING)
		
	if stats.heat <  15:
		warnings_to_activate.append(Warnings.UNDERHEATING)
	
	##Pressure	
	if stat_get(Stats.PRESSURE) > stats.pressure * 1.5:
		warnings_to_activate.append(Warnings.OVERPRESSURE)
		
	if stat_get(Stats.PRESSURE) < stats.pressure * 0.35:
		warnings_to_activate.append(Warnings.UNDERPRESSURE)
	
	##Parsing
	for warning: Warnings in Warnings:
		##Evaluated as active and was NOT already active. Toggle ON
		if warning_is_active(warning) and not warnings_to_activate.has(warning):
			warning_set_active(warning, true)
		##Evaluated as NOT active but was active. Toggle OFF
		elif not warning_is_active(warning) and warnings_to_activate.has(warning):
			warning_set_active(warning, false)
		
		
func warning_set_active(warning: Warnings, active: bool):
	dict_warnings[warning] = active
	if active:
		warning_activated.emit(warning)
	else:
		warning_deactivated.emit(warning)
		
func warning_is_active(warning: Warnings) -> bool:
	var active: bool = dict_warnings.get(warning, false)
	return active
#endregion


###CONTROL INPUT SYSTEM###
func on_control_activated(inputted: ControlInputs):
	match inputted:
		ControlInputs.TOGGLE_WARNING_LIGHTS:
			var activate: bool = !memory.get(&"WARNING_LIGHTS", false)
			toggle_warning_lights(activate)
			memory[&"WARNING_LIGHTS"] = activate
		
		ControlInputs.OPEN_EXTERNAL_DOORS:
			var activate: bool = !memory.get(&"EXTERNAL_DOOR", false)
			open_external_doors(activate)
			memory[&"EXTERNAL_DOOR"] = activate
			
			
###TEMP###

func toggle_warning_lights(activate: bool):
	for light: ShipWarningLight in part_warning_lights:
		part_toggle(light, activate)


func open_external_doors(open: bool):
	for door: ShipDoor in part_doors:
		part_toggle(door, open)
		
