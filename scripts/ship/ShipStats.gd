extends Resource
class_name ShipStats

@export var hull_max: float

@export var power_max: float

@export var heat_max: float

@export var cargo_max: float

@export var pressure_max: float


