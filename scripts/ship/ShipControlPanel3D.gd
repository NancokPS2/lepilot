extends VirtualTouchScreen3D
class_name ShipControlPanel3D

signal control_activated(inputted: Ship.ControlInputs)

func emit_control_activated(inputted: Ship.ControlInputs):
	control_activated.emit(inputted)
