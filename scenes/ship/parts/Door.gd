extends ShipPart
class_name ShipDoor

const OPEN_ANIMATION_NAME := &"open"
const CLOSE_ANIMATION_NAME := &"close"

@export var external: bool 
@export var door_animation_player: AnimationPlayer

func _ready() -> void:
	if not door_animation_player.has_animation(OPEN_ANIMATION_NAME) or not door_animation_player.has_animation(CLOSE_ANIMATION_NAME):
			push_error("Missing animation")
		

func toggle(activate: bool):
	if activate:
		door_animation_player.play(OPEN_ANIMATION_NAME)
	else:
		door_animation_player.play(CLOSE_ANIMATION_NAME)
