extends ShipPart
class_name ShipWarningLight

@export var attenuation_duration: float = 1
@export var peak_energy: float = 1.5
@export var light: OmniLight3D

var active_tween: Tween

func toggle(activate: bool):
	if is_instance_valid(active_tween):
		active_tween.kill()
		
	if activate:
		active_tween = create_tween()
		active_tween.tween_property(light, ^"light_energy", peak_energy, attenuation_duration)
		active_tween.tween_property(light, ^"light_energy", 0, attenuation_duration)
		active_tween.set_trans(Tween.TRANS_ELASTIC).set_loops(0)
	else:
		light.light_energy = 0

