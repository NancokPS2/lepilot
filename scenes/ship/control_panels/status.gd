extends ShipControlPanel3D

enum Status {
	STABLE,
	OVER,
	UNDER,
}
enum NodeTypes {
	LABEL,
	PROGRESS,
	MODULATOR,
}

const STABLE_COLOR := Color.GREEN
const OVER_COLOR := Color.RED
const UNDER_COLOR := Color.YELLOW

@onready var container: Container = $SubViewport/VBoxContainer
#@onready var heat_label: RichTextLabel = $SubViewport/VBoxContainer/Heat
#@onready var pressure_label: RichTextLabel = $SubViewport/VBoxContainer/Pressure

@export var ship: Ship:
	set(val):
		ship = val
		ship_update.call_deferred()

var node_arr: Array[Array]

func _ready() -> void:
	create_label_nodes(5)
	
func _process(delta: float) -> void:
	update_status(0,Ship.Stats.HULL)
	update_status(1,Ship.Stats.PRESSURE)
	update_status(2,Ship.Stats.HEAT)
	update_status(3,Ship.Stats.POWER_USAGE)
	update_status(4,Ship.Stats.CARGO)

func ship_update():
	#ship.warning_activated.connect(on_warning_change.bind(true))
	#ship.warning_deactivated.connect(on_warning_change.bind(false))
	pass

func create_label_nodes(count: int):
	for child: Node in container.get_children():
		child.queue_free()
	node_arr.clear()
	
	for index: int in count:
		var parent := Control.new()
		var label := Label.new()
		var setts := LabelSettings.new()
		var progress_bar := ProgressBar.new()
		var modulator_comp := ModulatorComponent.new()
		
		#Add nodes
		container.add_child(parent)
		parent.add_child(label)
		parent.add_child(progress_bar)
		
		##Parent setup
		parent.size_flags_vertical = Control.SIZE_EXPAND_FILL
		
		##Label setup
		label.label_settings = setts
		var average_size: float = (container.get_rect().size.x + container.get_rect().size.y) / 2
		label.label_settings.font_size = int(average_size / 10)
		label.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
		label.vertical_alignment = VERTICAL_ALIGNMENT_CENTER
		label.anchor_right = 1
		label.anchor_bottom = 1
		
		##Progress bar setup
		progress_bar.show_percentage = false
		progress_bar.modulate.a = 0.5
		progress_bar.anchor_right = 1
		progress_bar.anchor_bottom = 1
		progress_bar.fill_mode = ProgressBar.FILL_BOTTOM_TO_TOP
		
		node_arr.append([label, progress_bar, modulator_comp])
	

func get_status_node(index: int, type: NodeTypes) -> Control:
	if index > node_arr.size():
		push_error("Index {0} out of range".format([str(index)]))
	
	return node_arr[index][type]

func update_status(index: int, stat: Ship.Stats):
	const COLOR_GOOD := Color.GREEN
	const COLOR_NORMAL := Color.WHITE
	const COLOR_BAD := Color.RED
	
	var status_name: String = Ship.Stats.find_key(stat)
	var value: float = ship.stat_get(stat) * Constant.PRESSURE_KPA_VALUE
	var value_suffix: String = ""
	var value_low: float = 0
	var value_high: float = 100
	match stat:
		Ship.Stats.PRESSURE:
			value_suffix = Constant.PRESSURE_KPA_NAME
			value_low = Constant.THRESH_PRESSURE_LOW
			value_high = ship.stats.pressure_max
		Ship.Stats.HEAT:
			value_suffix = Constant.TEMPERATURE_K_NAME
			value_low = Constant.THRESH_TEMPERATURE_LOW
			value_high = ship.stats.heat_max

	##Modulation color
	var mid_value: float = (value_low + value_high) / 2
	var first_color: Color = COLOR_NORMAL
	var second_color: Color
	
	var normalized_val: float
	if value >= mid_value:
		second_color = COLOR_GOOD
		normalized_val = remap(value, mid_value, value_high, 0, 1)
	elif value < mid_value:
		second_color = COLOR_BAD
		normalized_val = remap(value, mid_value, value_low, 0, 1)
	var final_color: Color = first_color.lerp(second_color, normalized_val)
			
	##Label. Example: "Pressure: 80 kPa"
	var label: Label = get_status_node(index, NodeTypes.LABEL)
	label.text = "{0}: {1}{2}".format(
		[status_name, str(value), value_suffix]
	)
	
	##Label Settings	
	label.label_settings.font_color = final_color
	
	##Progress bar
	var progress_bar: ProgressBar = get_status_node(index, NodeTypes.PROGRESS)
	progress_bar.value = remap(
		value,
		value_low,
		value_high,
		0,
		100
	)
	progress_bar.modulate = final_color
	

#func on_warning_change(warning: Ship.Warnings, active: bool):
	#if active:
		#match warning:
			#Ship.Warnings.OVERPRESSURE:
				#pressure_label.modulate = OVER_COLOR
				#
			#Ship.Warnings.UNDERPRESSURE:
				#pressure_label.modulate = UNDER_COLOR
								#
			#Ship.Warnings.OVERHEATING:
				#heat_label.modulate = OVER_COLOR
				#
			#Ship.Warnings.UNDERHEATING:
				#heat_label.modulate = UNDER_COLOR
	#
	#else: 
		#match warning:
			#Ship.Warnings.OVERPRESSURE:
				#pressure_label.modulate = STABLE_COLOR
				#
			#Ship.Warnings.UNDERPRESSURE:
				#pressure_label.modulate = STABLE_COLOR
				#
			#Ship.Warnings.OVERHEATING:
				#heat_label.modulate = STABLE_COLOR
				#
			#Ship.Warnings.UNDERHEATING:
				#heat_label.modulate = STABLE_COLOR
