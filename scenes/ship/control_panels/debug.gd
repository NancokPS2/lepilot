extends ShipControlPanel3D

@onready var btn_warning_lights: Button = $SubViewport/WarningLights

func _ready() -> void:
	super._ready()
	btn_warning_lights.pressed.connect(emit_control_activated.bind(Ship.ControlInputs.TOGGLE_WARNING_LIGHTS))
	btn_warning_lights.mouse_entered.connect(printer.bind("INSIDE"))
	#print(btn_warning_lights.get_rect())

func printer(val):
	print(val)

	
