extends Control

@onready var node_label: Label = $Label
@onready var node_progress_bar: Range = $ProgressBar
@onready var node_modulator_component: ModulatorComponent = $ModulatorComponent

@export var status_name: String
@export var value: float 
@export var value_suffix: String

@export var value_high: float
@export var value_low: float

func _ready() -> void:
	node_label.label_settings = LabelSettings.new()
	resized.connect(update_size_change)
	
	update_modulator_path()
	update_size_change()
	update_high_low_change()
	update_value_change()


func update_modulator_path():
	node_modulator_component.object_reference = self
	node_modulator_component.object_property_path = ^"value"


func update_value_change():
	##Example: "Pressure: 80 kPa"
	node_label.text = "{0}: {1}{2}".format(
		[status_name, str(value), value_suffix]
	)
	
	node_progress_bar.value = remap(
		node_progress_bar.value,
		value_low,
		value_high,
		0,
		100
	)


func update_high_low_change():
	node_modulator_component.thres_high = value_high
	node_modulator_component.thres_low = value_low
	

func update_size_change():
	var average_size: float = (get_rect().size.x + get_rect().size.y) / 2
	node_label.label_settings.font_size = int(average_size / 10)
	

